FROM python:3.6.7
RUN mkdir /app
WORKDIR /app
COPY requirements.txt .
RUN pip3 install --no-cache-dir -r requirements.txt
COPY . .
EXPOSE 5000
ENTRYPOINT  ["gunicorn", "--config", "gunicorn_config.py", "wsgi:app"] 
